// ==UserScript==
// @name            YouTube Channel Blocker
// @namespace       https://github.com/ntorgov/GSL
// @version         0.39
// @description     Удаляет нежелательные каналы из предлагаемого просмотра, дабы глаз не мозолили
// @author          ntorgov
// @run-at          document-end
// @icon            http://s.ytimg.com/yts/img/favicon-vfldLzJxy.ico
// @match           https://www.youtube.com/*
// @match           http://www.youtube.com/*
// @grant           none
// @require         https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @downloadUrl     https://github.com/ntorgov/GSL/raw/master/YoutubeChannelBlock.user.js
// @updateUrl       https://github.com/ntorgov/GSL/raw/master/YoutubeChannelBlock.user.js
// ==/UserScript==

var channelList = [
	'UCIrVNf-KP7XbSWmFabU1itA', // TheTu33               - Какая-то херь с лезгинкой, как вообще это попадает в рекомендованное
	'UCG9fNpYq7nrPa84d4f0AdUg', // velikentli terekemeli - Еще одна фигня про дагестанцев с лезгинками
	'UCTcEMaEXVH8bNE67d5JDjcQ', // Joombler Dota         - Dota
	'UCUtdmXEDHdSHPZP37n_Bymw', // viperkeeper           - Какое-то дерьмо про змей. Мерзость.
	'UCWnUzo23QNUeB7FgUspM-TQ', // Dota Boss             - Дота, очередная
	'UCfDGIRe7OzzD_5jJTMz85Iw', // Олег Сорк             - Какой-то геймер
	'UCVj6yIrGoZEqemL7xXHDXiQ', // PositiveDota          - Дота, может их уже стоит убирать просто по названию?
	'UCedqnsVXjFlao53mKg7mnkQ', // DocWitson             - Очередной геймер
	'UCg-yksUSv1pe9c5UcjEhRnA', // TheBestForDota        - Господи, за что???
	'UCrDkAvwZum-UTjHmzDI2iIw', // officialpsy           - Узкоглазый хрен скачет, изображая наездника
	'UC-27_Szq7BtHDoC0R2U0zxA', // AdamThomasMoran       - +100500, глупый х.й кривляется перед камерой.
	'UCPT9_sNLoBLjH1uea7zpVIA', // This is Хорошо        - Еще один глупый х.й
	'UCYjgEmnqwdiiiSVAXgNX9rA', // BestDOTA2streams      - Опять Дота
	'UCmR-oFhgGGe-dCRiDg2ysLw', // waveya2011            - Корейцы с лошадьми
	'UCWvojFEaSodoKG7Z-ZZo96A', // SokoL[off] TV         - Жирный мудозвон
	'UCMu5gPmKp5av0QCAajKTMhw', // ERB                   - Реп
	'UC6bTF68IAV1okfRfwXIP1Cg', // itpedia               - Охинея какая-то
	'UCQeaXcwLUDeRoNVThZXLkmw', // Большой тест-драйв    - Раздражают
	'UCU8S5gNCh5XP14LfI3oAMnA', // MerrySweetBerry       - Тупость какая-то
	'UC8A6H08iqFxw6gTvIUNKOWw', // Julia Melman          - Тупость
	'UC87bIQI71SF1wW1J16BerkA', // Последние Новости     - Охинея
	'UCbSdb-pzDPdhmjb7iY2hySQ', // DESERTOD TV           - Дерьмищще
	'UCkyrSWEcjZKpIwMxiPfOcgg', // 5 канал               - Что-то с хохлами
	'UCD8cMdbEEcdIJsVtvwm836w', // Агния Огонек          - Дура какая-то
	'UCBLfY10VWpGZ42sjVFVBdLg', // mmoboom               - Геймер тупой
	'UCFYaJYrGQAWuotpzTXWzucg', // Naur                  - Тупиздень
	'UCw7aITWeEbQrVQ1wP88p-SA', // sekretmesto           - Трепло бестолковое
	'UCX7NRvQF3UWN350lS_mSn9A', // SuperMirage2 Mirag    - Что-то про хохлов
	'UCow8fRnE3USmaB_aKNmdoiQ', // Элорд                 - GTA, странно что вообще в это кто-то играет
	'UCMEiyV8N2J93GdPNltPYM6w', // Espreso.TV            - Хохло-тв
	'UC2oGvjIJwxn1KeZR3JtE-uQ', // Громадське Телебачення- Как это вообще оказывается в рекомендованном?
	'UCIh05bP0r0qS0n01ng0jOVQ', // LB ua                 - Хохлы
	'UC-wWyFdk_txbZV8FKEk0V8A', // Радіо Свобода         - Снова хохлы
	'UCie5kwnM_xv4gUtTBqC_uLw', // Nevextv               - Хохлосрач
    'UC2Ru64PHqW4FxoP0xhQRvJg', // Caramba Topless       - Очередной даун кривляется перед камерой
    'UCJSkz1aAr7-XDo_sdNIIp3g', // Бабкины Яйца          - Хрень какая-то
    'UCoW8lm2MENYjBiktAhKa1sQ', // VIKI NIKI             - Вот как это попало в рекомендации?
    'UCK4K2-u8aXrVbyvmL7UZKGA', // Лёха Кайфовщик        - Как??? Откуда??? За что???
    'UCrmyJu30BCX5yBMj_V349JQ', // Лучшие Ролики         - Идиотизм
    'UCyqF4gkSOvYFVhs-ciOY0GQ', // Дункан Маклауд        - Что-то опять про хохлов
    'UCRvb9G_oeHy_8ezjIsDTYNQ', // Ржачный видос         - Жрать, ржать, срать...
    'UCNYqz86EVyRjekzPmW8nJqQ', // «Quo vadis»           - Какая-то хрень про блядей
    'UC0mp0w9uWQxrliQJ8W9G2Pg', // pravdavideo           - Очередная "ржака" для даунов
    'UCtKnY1vJ37v-KPgejc2kkBQ', // Канал Украина         - Из названия все понятно
    'UCOjjJFywLQTzZcARKsTUzXA', // DaiFiveTop            - Почему бы ютуб не сделать платным? Чтоб было поменьше токого
	'UC_vU2naC2IAUCr0LnDbnRjg', // Рассвет.ТВ            - Хохлы
	]; 
	
var clearRecomendsInView = function() {
	jQuery("li.video-list-item.related-list-item").each(function(){
		var channel = jQuery(this).find("span.g-hovercard");
		if(typeof(channel) != 'undefined') {
			var channelCode = channel.attr("data-ytid");
			for(n=0; n<channelList.length; n++) {
				if(channelCode == channelList[n]) {
					jQuery(this).remove();
				}
			}
		}
	});
};

// Фунция очистки рекомендуемого
var clearRecomends = function() {
	jQuery("li.yt-shelf-grid-item").each(function() {
		var channelSimpleRoot = jQuery(this).find("div.yt-lockup-content");
		var channelElement = channelSimpleRoot.find("a.g-hovercard");
		if(typeof(channelElement) != 'undefined') {
			var channelCode = channelElement.attr("data-ytid");
			for(n = 0; n < channelList.length; n++) {
				if(channelCode == channelList[n]) {
					var button = channelSimpleRoot.find("button.yt-ui-menu-item");
					var dataToken = button.attr("data-feedback-token");
					jQuery("button[data-feedback-token='"+dataToken+"']").click();
				}
			}
		}
	});
    jQuery("li.expanded-shelf-content-item-wrapper").each(function() {
        var channelSimpleRoot = jQuery(this).find("div.yt-lockup-content");
        var channelElement = channelSimpleRoot.find("a.g-hovercard");
        if(typeof(channelElement) != 'undefined') {
            var channelCode = channelElement.attr("data-ytid");
            for(n = 0; n < channelList.length; n++) {
                if(channelCode == channelList[n]) {
                    var button = channelSimpleRoot.find("button.yt-ui-menu-item");
                    var dataToken = button.attr("data-feedback-token");
                    jQuery("button[data-feedback-token='"+dataToken+"']").click();
                }
            }
        }
    });
};

var dislikeVideo = function() {
	var videoHeader = jQuery("div#watch-header");
	var channelCode = videoHeader.find("div.yt-user-info").children("a.g-hovercard").attr("data-ytid");
	for(n = 0; n < channelList.length; n++) {
		if(channelCode == channelList[n]) {
			var dislikeButton = document.getElementById("watch-dislike");
			if(typeof(jQuery(dislikeButton).attr("aria-pressed")) =='undefined') {
				dislikeButton.click();
			} 
		}
	}
};

if(window.location.origin == "https://www.youtube.com" || window.location.origin == "http://www.youtube.com") {
	// Не спрашивай зачем :)
	jQuery("a.spf-link").removeClass("spf-link");
	
	if(window.location.pathname == "/watch") {
		setTimeout(dislikeVideo(), 3000);
		setInterval(clearRecomendsInView, 5000);
	} else {
		setInterval(clearRecomends, 5000);
	}
}