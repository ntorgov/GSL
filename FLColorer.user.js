// ==UserScript==
// @name            Colorer for fl.ru
// @namespace       https://github.com/ntorgov/GSL
// @version         0.4
// @description     Подкрашивает сайт для большей информативности fl.ru
// @author			ntorgov
// @icon            http://www.fl.ru/favicon.ico
// @match           https://www.fl.ru/proj/*
// @match           http://www.fl.ru/proj/*
// @run-at          document-body
// @grant           none
// @require         https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @downloadUrl     https://github.com/ntorgov/GSL/raw/master/FLColorer.user.js
// @updateUrl       https://github.com/ntorgov/GSL/raw/master/FLColorer.user.js
// ==/UserScript==

// Красим в зеленоватый заказы где я исполнитель
jQuery("span.fps2").parent().parent().each(function() {
	jQuery(this).css("border-radius", "10px").css("padding-left", "10px").css("padding-right", "10px").css("background-color", "#d9ffe3");
});

// Красим в желтоватый заказы где я кандидат
jQuery("span.fps3").parent().parent().each(function() {
	jQuery(this).css("border-radius", "10px").css("padding-left", "10px").css("padding-right", "10px").css("background-color", "#fcffd9");
});

// Красим в голубой, если есть новые сообщения
jQuery("span a.b-layout__link_bold").parent().parent().parent().parent(".b-post").each(function(){
	jQuery(this).css("border-radius", "10px").css("padding-left", "10px").css("padding-right", "10px").css("background-color", "#d9deff");
});