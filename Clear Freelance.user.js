// ==UserScript==
// @name            Clear Freelance.ru
// @namespace       https://github.com/ntorgov/GSL
// @version         0.32
// @description     Производит очистку страниц freelance.ru от всякого говна
// @author          ntorgov
// @icon            https://freelance.ru/favicon.ico
// @match           https://freelance.ru/
// @match           https://freelance.ru/projects/*
// @match           https://freelance.ru/setup/*
// @run-at          document-end
// @grant           none
// @require         https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @downloadUrl     https://github.com/ntorgov/GSL/raw/master/Clear%20Freelance.user.js
// @updateUrl       https://github.com/ntorgov/GSL/raw/master/Clear%20Freelance.user.js
// ==/UserScript==

var currentUrl = document.location.origin;
var mainPage = "https://freelance.ru";

//	Замена ссылки в лого на абсолютную
jQuery("div.logotype").children("a").attr("href", mainPage);

jQuery("div.f_av_block").remove();
var header = jQuery("div#header");
header.css("margin-top", "0");
header.children("div.row.no-margin-x").remove();
header.children("div.row.hidden-xs.hidden-sm").css("margin-bottom", "0");

//	Удаление блока с рекламой в шапке
jQuery("div.lama").remove();
jQuery("div.lama980x60").remove();
jQuery("div.lama600x90").remove();
jQuery("div.banner_240x400").remove();

//	Удаление блока с рекламой "Fair Play"
jQuery("div.fplay").remove();

if(currentUrl == mainPage || currentUrl == mainPage + "/") {
	//	Удаление блоков обращения к заказчику и фрилансеру
	jQuery("section#for_fl").remove();
	jQuery("section#for_empl").remove();
	
	//	Удаление двух бестолковых блоков в подвале
	jQuery("div.block_h.funny.block_hyphen").remove();
	
	//	Удаление блока с проектами в подвале
	jQuery("section.other_projects").remove();
	
	//	Удаление левого блока со всякими уродами
	jQuery("div#col_1").remove();
	
	//	Растягивание правого блока на всю ширину
	jQuery("div#col_center_mainpage").removeClass("col-lg-offset-1").removeClass("col-lg-8").addClass("col-lg-12");
	
	//	Замена даты в записях на адекватное значение
	jQuery("li.pdata").each(function(){
		jQuery(this).text(jQuery(this).attr("title"));
	});
	//	Убираем коммерческие заявки, ибо толку от них нет инфига
	jQuery("div.proj.not_public").each(function() {
		jQuery(this).children("ul.list-inline").hide(2000);
		jQuery(this).children("a.descr").delay(2000).hide(2000);
	}).delay(5000).hide(2000);
}

//	Временное сокрытие формы с фильтром проектов
jQuery("div.filter_button").css("margin-top","15px");
var projectFilter = jQuery("form#proj_filter_form");
projectFilter.children("h4").css("cursor", "pointer");
projectFilter.children("fieldset").hide(500);
projectFilter.children("h4").click(function(){
	var fieldsetElement = jQuery("form#proj_filter_form").children("fieldset");
	if(fieldsetElement.attr("style") =="display: none;") {
		fieldsetElement.show(500);
	} else {
		fieldsetElement.hide(500);
	}
});

//	Удалене блока 18+ в подвале. На кой он вообще там?
jQuery("div.plus18").remove();