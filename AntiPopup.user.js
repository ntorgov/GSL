// ==UserScript==
// @name					Anti Popup
// @namespace			https://github.com/ntorgov/GSL
// @version					0.1
// @description			Уничтожает некоторые popup блоки и скрипты
// @author					ntorgov
// @icon						https://freelance.ru/favicon.ico
// @match					*
// @run-at					document-end
// @grant						none
// @downloadUrl			https://github.com/ntorgov/GSL/raw/master/AntiPopup.user.js
// @updateUrl				https://github.com/ntorgov/GSL/raw/master/AntiPopup.user.js
// ==/UserScript==

var scriptsPatterns = [
		/<!-- BEGIN JIVOSITE CODE -->([\s\S]*?)<!-- END JIVOSITE CODE -->/img,		// Чат от jivosite
	];
var blocksIds = [
		'jivo_top_wrap',
		'jivo_css_detector'
	];
var blocksClasses = [
		'cmv_phone'
	];	
	
var eraseCounter = 5
var regexpErased = false;
	
var cleanScripts = function() {
	if(regexpErased == false) {
		for(var n = 0; n < scriptsPatterns.length; n++) {
			document.head.innerHTML = document.head.innerHTML.replace(scriptsPatterns[n], "<!-- Removed -->");
			document.body.innerHTML = document.body.innerHTML.replace(scriptsPatterns[n], "<!-- Removed -->");
		}
		regexpErased = true;
	}
	var erased = 0;
	for(var n = 0; n < blocksIds.length; n++) {
		var element = document.getElementById(blocksIds[n]);
		//console.log(element + " " + blocksIds[n]);
		if(typeof(element) != 'undefined' && element != null) {
			element.outerHTML = "";
			erased = 1;
		} 
	}
	for(var n = 0; n < blocksClasses.length; n++) {
		var elements = document.getElementsByClassName(blocksClasses[n]);
		//console.log(element + " " + blocksClasses[n]);
		for(var i=0; i<elements.length; i++) {
			if(typeof(elements[i]) != 'undefined' && elements[i] != null) {
				elements[i].outerHTML = "";
				erased = 1;
			} 
		}
	}
	if(erased == 1) {
		eraseCounter++;
	} else {
		eraseCounter--;
	}
	if(eraseCounter <= 0) {
		clearInterval(ClearTimer);
	}
};

cleanScripts();
var ClearTimer = setInterval(cleanScripts, 1000);