// ==UserScript==
// @name            YouTube Auto HD Quality
// @namespace       https://github.com/ntorgov/GSL
// @version         0.11
// @description     Устанавливает автоматом HD качество для роликов
// @author          ntorgov
// @run-at          document-end
// @icon            http://s.ytimg.com/yts/img/favicon-vfldLzJxy.ico
// @match           https://www.youtube.com/watch*
// @match           http://www.youtube.com/watch*
// @grant           none
// @require         https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @downloadUrl     https://github.com/ntorgov/GSL/raw/master/YoutubeAutoHD.user.js
// @updateUrl       https://github.com/ntorgov/GSL/raw/master/YoutubeAutoHD.user.js
// ==/UserScript==

if(typeof(ytplayer) != 'undefined') {
	var cnfg = ytplayer.config;
	cnfg.args.vq="hd1080+";
	ytplayer.load(cnfg);
	jQuery("div.ytp-button[role='option'][tabindex='2700']").first().click();
}