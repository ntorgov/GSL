// ==UserScript==
// @name            Endomondo Time Setter
// @namespace       https://github.com/ntorgov/GSL
// @version         0.6
// @description     Автоматически счиает время при ручном вводе тренировки
// @author          ntorgov
// @run-at          document-end
// @icon            http://www.endomondo.com/favicon.ico
// @match           https://www.endomondo.com/?wicket:interface=:*
// @grant           none
// @require         https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @downloadUrl     https://github.com/ntorgov/GSL/raw/master/Endomondo.user.js
// @updateUrl       https://github.com/ntorgov/GSL/raw/master/Endomondo.user.js
// ==/UserScript==
var MS_PER_MINUTE = 60000;
var MS_PER_HOUR = MS_PER_MINUTE * 60;
var nowTime = new Date;

var durationHours = jQuery("input[name$='duration:hoursField']");
var durationMinutes = jQuery("input[name$='duration:minutesField']");
var durationSeconds = jQuery("input[name$='duration:secondsField']");

var startHours = jQuery("input[name$='startTime:hoursField']");
var startMinutes = jQuery("input[name$='startTime:minutesField']");

var nowHours = nowTime.getHours();
var nowMinutes = nowTime.getMinutes();

startHours.val(nowHours);
startMinutes.val(nowMinutes);

durationSeconds.change(function() {
	Calculate();
});

durationMinutes.change(function() {
	Calculate();
});

durationHours.change(function() {
	Calculate();
});

var Calculate = function() {
	var durationSecondsValue = durationSeconds.val();
	var durationMinutesValue = durationMinutes.val();
	var durationHoursValue = durationHours.val();
	
	var newDate = new Date(nowTime - (durationHoursValue * MS_PER_HOUR) - (durationMinutesValue * MS_PER_MINUTE) - durationSecondsValue);
	
	startMinutes.val(newDate.getMinutes());
	startHours.val(newDate.getHours());
};