// ==UserScript==
// @name            Clear FL.ru Adsense
// @namespace       https://github.com/ntorgov/GSL
// @version         0.6
// @description     Удаляет ненужную рекламу фрилансеров со страниц на сайте fl.ru
// @author			ntorgov
// @icon            http://www.fl.ru/favicon.ico
// @match           https://www.fl.ru/projects/
// @match           http://www.fl.ru/projects/
// @match           https://www.fl.ru/konkurs/
// @match           http://www.fl.ru/konkurs/
// @run-at          document-body
// @grant           none
// @require         https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @downloadUrl     https://github.com/ntorgov/GSL/raw/master/FLAdsense.user.js
// @updateUrl       https://github.com/ntorgov/GSL/raw/master/FLAdsense.user.js
// ==/UserScript==

// Убираем левый блок с фрилансерами
jQuery("div#pay_place_left").remove();