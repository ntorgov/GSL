// ==UserScript==
// @name				Mail.ru Composer Editor
// @namespace		https://github.com/ntorgov/GSL
// @version				0.10.1
// @description		Скрипт очищает нежелательные строки в редакторе ответов на письма в вебинтерфейсе mail.ru и также облегчает создание ответов на письма автоматизирую рутинные операции
// @author				ntorgov
// @icon					https://img.imgsmail.ru/r/default/favicon.ico
// @match				https://e.mail.ru/*
// @match				http://e.mail.ru/*
// @run-at				document-end
// @unwrap
// @grant       			none
// @require				https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js
// @downloadUrl		https://github.com/ntorgov/GSL/raw/master/Mail.ru.Composer.Cleaner.user.js
// @updateUrl			https://github.com/ntorgov/GSL/raw/master/Mail.ru.Composer.Cleaner.user.js
// ==/UserScript==

    needToHello = true;		// Установите false, если здороваться не нужно.

    unsafeWindow.helloMessage = "Здравствуйте!";		//	Текст приветствия
    unsafeWindow.startString = "Всё что следует за строчкой";
    unsafeWindow.endString = "клиента:";
    unsafeWindow.isInComposer = false;
    unsafeWindow.instanceRun = false;
	
	var composerId = null;

    var clearString = function() {
		var contentCleared = false;
		var composer = tinyMCE.get(composerId);
		var htmlContent = composer.getContent(); 
		if((htmlContent.indexOf(startString) != -1) && (htmlContent.indexOf(endString) != -1)) {
			while(htmlContent.indexOf(startString) != -1 && htmlContent.indexOf(endString) != -1) {
				var startPosition = htmlContent.indexOf(startString);
				var endPosition = htmlContent.indexOf(endString) + endString.length;

				var firstPart = htmlContent.substr(0, startPosition);
				var secondPart = htmlContent.substr(endPosition, htmlContent.length);

				if(startPosition > 0 && endPosition > 0) {
					htmlContent = firstPart + secondPart;
					contentCleared = true;
				}
			}
		} else {
			contentCleared = true;
		}

		if(contentCleared == true) {
			composer.setContent(htmlContent);
			console.log('Ready to hello');
			if(isInComposer == false) {
				sayHello();
			}
		} else {
			instanceRun = false;
		}
	};

	var sayHello = function() {
		if(needToHello == true) {
			isInComposer = true;
			var composer = tinyMCE.get(composerId);
			composer.execCommand('mceInsertContent', false, helloMessage + "<br /><br /><br />" );
		}
		instanceRun = false;
	}

	var DetectFrame = function() {
		console.log("DetectFrame: InstanceRun: " + instanceRun);
		
		if(window.location.pathname.indexOf("compose") >= 1) {
			if(instanceRun === false) {
			instanceRun = true;
				var frame = jQuery("iframe[id*='composeEditor']");
				if(frame != null && typeof(frame) != "undefined") {
					console.log("DetectFrame: InComposer: " + instanceRun);
					var iframeId = frame.attr("id");
					if(iframeId != null && typeof(iframeId) != "undefined") {

						composerId = iframeId.substr(0, iframeId.length-4);
						composer = tinyMCE.get(composerId);
						if(composer != null && typeof(composer) == "object") {
							console.log("Before Start");
							setTimeout(clearString, 500);
						} else {
							instanceRun = false;
						}
					} else {
						instanceRun = false;
					}
				} else {
					instanceRun = false;
				}
			}
		} else {
			isInComposer = false;
			instanceRun = false;
		}
	}

	if(typeof(MainProcessTimer) == 'undefined') {
		console.log("Hello, World!");
		//GM_setValue("InstanceCheck", true);
		unsafeWindow.MainProcessTimer = setInterval(DetectFrame, 500);
	}