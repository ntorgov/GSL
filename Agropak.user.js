// ==UserScript==
// @name            Agropak Style Fix
// @namespace       https://github.com/ntorgov/GSL
// @version         0.4
// @description     Слегка меняет оформление сайта, делая его немного удобней
// @author          ntorgov
// @run-at          document-end
// @icon            http://www.agropak.net/favicon.ico
// @match           http://www.agropak.net/*
// @grant           none
// @require         https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js
// @downloadUrl     https://github.com/ntorgov/GSL/raw/master/Agropak.user.js
// @updateUrl       https://github.com/ntorgov/GSL/raw/master/Agropak.user.js
// ==/UserScript==

jQuery("ul.menu").css("text-transform", "none");
jQuery("li.menu-level-1").css("padding-left", "10px").css("text-overflow", "ellipsis").css("width", "209px").css("overflow", "hidden").css("white-space", "nowrap");